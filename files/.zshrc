# .zshrc 中 p9k 的配置内容
#####################################
# P9k 配色方案： bright、 light、dark（模式下右侧提示符不显示）
POWERLEVEL9K_COLOR_SCHEME='light'

# 在新提示符之前插入一行以保持间距
POWERLEVEL9K_PROMPT_ADD_NEWLINE=true

# https://github.com/bhilburn/powerlevel9k/tree/next#customizing-prompt-segments
# LEFT_PROMPT 左侧提示符
# ===========================================================
POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(os_icon time_joined dir dir_writable  docker_machine vcs background_jobs status)
# POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(os_icon time dir dir_writable vcs status background_jobs)
# 光标另起一行，false 明确禁用， true启用
POWERLEVEL9K_PROMPT_ON_NEWLINE=true
# 第一行左提示符前缀
POWERLEVEL9K_MULTILINE_FIRST_PROMPT_PREFIX=""
# 最后一行左提示符前缀符号
POWERLEVEL9K_MULTILINE_LAST_PROMPT_PREFIX="$ "
# 另一个示列，直接使用zsh提供的配置方式
#POWERLEVEL9K_MULTILINE_LAST_PROMPT_PREFIX="%K{white}%F{black} \UE12E `date +%T` %f%k%F{white}%f "
# 为每个分段之间设置间距，不然太挤了
POWERLEVEL9K_WHITESPACE_BETWEEN_LEFT_SEGMENTS=' '
# 定制左侧分段分隔符separator，默认箭头
#POWERLEVEL9K_LEFT_SEGMENT_SEPARATOR=''
# 子分隔符
#POWERLEVEL9K_LEFT_SUBSEGMENT_SEPARATOR=''

# RIGHT_PROMP 右侧提示符
# Why does the prompt on the right overflow to the next line?
# ===========================================================
#POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(vcs  status  background_jobs status)
POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=( vi_mode )
# 禁用右侧提示符
#POWERLEVEL9K_DISABLE_RPROMPT=true
# 分段分隔符separator
#POWERLEVEL9K_RIGHT_SEGMENT_SEPARATOR=''
#POWERLEVEL9K_RIGHT_SUBSEGMENT_SEPARATOR=' '



## dir 目录
# https://github.com/bhilburn/powerlevel9k/blob/next/segments/dir/README.md
#======================================
# 截断策略
# -------------
# 缩短目录时使用的符号，默认为 … 
POWERLEVEL9K_SHORTEN_DELIMITER=''
# 目录截断策略一：从中间开始截断，前后都保留2字符：/Fe…lx/
#POWERLEVEL9K_SHORTEN_STRATEGY="truncate_middle"
#POWERLEVEL9K_SHORTEN_DIR_LENGTH=2
# 截断策略二： 从右端开始截断保留2个字符，效果：/Fe…/
#POWERLEVEL9K_SHORTEN_STRATEGY="truncate_from_right"
#POWERLEVEL9K_SHORTEN_DIR_LENGTH=2
# 截断策略三：效果：/mnt/d/…/…/Blog/fandean
POWERLEVEL9K_SHORTEN_STRATEGY='truncate_to_first_and_last'
POWERLEVEL9K_SHORTEN_DIR_LENGTH=2
# 禁用文件夹图标，将其设置为空字符串
# ------------
#POWERLEVEL9K_ETC_ICON=''
#POWERLEVEL9K_FOLDER_ICON=''
#POWERLEVEL9K_HOME_ICON=''
#POWERLEVEL9K_HOME_SUB_ICON=''
# 目录颜色： none表示无
# -------------
#POWERLEVEL9K_DIR_ETC_BACKGROUND='none'
#POWERLEVEL9K_DIR_ETC_FOREGROUND='005'
#POWERLEVEL9K_DIR_HOME_BACKGROUND='none'
#POWERLEVEL9K_DIR_HOME_FOREGROUND='004'
#POWERLEVEL9K_DIR_DEFAULT_BACKGROUND='none'
POWERLEVEL9K_DIR_DEFAULT_FOREGROUND='grey85'
#POWERLEVEL9K_DIR_HOME_SUBFOLDER_BACKGROUND='none'
#POWERLEVEL9K_DIR_HOME_SUBFOLDER_FOREGROUND='004'


# time 时间
# ---------------
POWERLEVEL9K_TIME_FORMAT="%D{%m/%d %H:%M}"
# 是否显示时钟图标
POWERLEVEL9K_TIME_ICON=''
# 背景色
POWERLEVEL9K_TIME_BACKGROUND='white'
# 前景色
POWERLEVEL9K_TIME_FOREGROUND='006'


# os_icon custom 操作系统图标定制，none表示无
# ----------------
POWERLEVEL9K_OS_ICON_BACKGROUND='white'
POWERLEVEL9K_OS_ICON_FOREGROUND='blue1'
# 在操作系统图标后添加空格以增加间隙
POWERLEVEL9K_WINDOWS_ICON=' '
#POWERLEVEL9K_LINUX_UBUNTU_ICON=' '
# 强制将Linux图标变为Win图标（个人喜好，因为我是在WSL中）
POWERLEVEL9K_LINUX_UBUNTU_ICON=' '

# vcs git仓库配置
# https://github.com/bhilburn/powerlevel9k/blob/next/segments/vcs/README.md
#-------------------
# 显示提交信息
#POWERLEVEL9K_SHOW_CHANGESET=true
#POWERLEVEL9K_CHANGESET_HASH_LENGTH=4
# 颜色配置
POWERLEVEL9K_VCS_CLEAN_BACKGROUND='springgreen1'    # clean干净 green
POWERLEVEL9K_VCS_CLEAN_FOREGROUND='skyblue1'
POWERLEVEL9K_VCS_UNTRACKED_BACKGROUND='orange3' # 未跟踪
POWERLEVEL9K_VCS_UNTRACKED_FOREGROUND='grey85'
POWERLEVEL9K_VCS_MODIFIED_BACKGROUND='yellow'  # 已修改
POWERLEVEL9K_VCS_MODIFIED_FOREGROUND='black'

#POWERLEVEL9K_VCS_GIT_HOOKS=(vcs-detect-changes git-untracked git-aheadbehind git-remotebranch git-tagname)



# status
# https://github.com/bhilburn/powerlevel9k/blob/next/segments/status/README.md
# true 如果命令执行失败则显示
POWERLEVEL9K_STATUS_CROSS=true
# false如果命令执行成功则隐藏不显示(如果仅仅回车已经出现的信息不会消失)
POWERLEVEL9K_STATUS_OK=false
POWERLEVEL9K_STATUS_OK_BACKGROUND="clear"
POWERLEVEL9K_STATUS_OK_FOREGROUND="cyan3"
POWERLEVEL9K_STATUS_ERROR_BACKGROUND="clear"
POWERLEVEL9K_STATUS_ERROR_FOREGROUND="red"

# background_jobs
# https://github.com/bhilburn/powerlevel9k/blob/next/segments/background_jobs/README.md
#POWERLEVEL9K_BACKGROUND_JOBS_VERBOSE_ALWAYS='true'    # 总是显示
#POWERLEVEL9K_BACKGROUND_JOBS_VERBOSE=true             # 如果有多个后台进程则显示数量

# root 指示器
# https://github.com/bhilburn/powerlevel9k/blob/next/segments/root_indicator/README.md
#POWERLEVEL9K_ROOT_INDICATOR_BACKGROUND="red"
#POWERLEVEL9K_ROOT_INDICATOR_FOREGROUND="white"

# custom_newline 这个名称就是一个分段名称，直接写入分段列表即可使用
#POWERLEVEL9K_CUSTOM_NEWLINE="print '\n'"
#POWERLEVEL9K_CUSTOM_NEWLINE_BACKGROUND="green"
#POWERLEVEL9K_CUSTOM_NEWLINE_FOREGROUND="white"

# Set 'context' segment colors
# https://github.com/bhilburn/powerlevel9k/blob/next/segments/context/README.md
#POWERLEVEL9K_CONTEXT_DEFAULT_FOREGROUND=$P9KGT_YELLOW
#POWERLEVEL9K_CONTEXT_ROOT_FOREGROUND=$P9KGT_YELLOW
#POWERLEVEL9K_CONTEXT_SUDO_FOREGROUND=$P9KGT_YELLOW
#POWERLEVEL9K_CONTEXT_REMOTE_FOREGROUND=$P9KGT_YELLOW
#POWERLEVEL9K_CONTEXT_REMOTE_SUDO_FOREGROUND=$P9KGT_YELLOW
#POWERLEVEL9K_CONTEXT_DEFAULT_BACKGROUND=$P9KGT_TERMINAL_BACKGROUND
#POWERLEVEL9K_CONTEXT_ROOT_BACKGROUND=$P9KGT_TERMINAL_BACKGROUND
#POWERLEVEL9K_CONTEXT_SUDO_BACKGROUND=$P9KGT_TERMINAL_BACKGROUND
#POWERLEVEL9K_CONTEXT_REMOTE_BACKGROUND=$P9KGT_TERMINAL_BACKGROUND
#POWERLEVEL9K_CONTEXT_REMOTE_SUDO_BACKGROUND=$P9KGT_TERMINAL_BACKGROUND

# Set 'dir_writable' segment colors
# https://github.com/bhilburn/powerlevel9k/blob/next/segments/dir_writable/README.md
#POWERLEVEL9K_DIR_WRITABLE_FORBIDDEN_FOREGROUND=$P9KGT_YELLOW
#POWERLEVEL9K_DIR_WRITABLE_FORBIDDEN_BACKGROUND=$P9KGT_RED

# VI Mode
# https://github.com/Powerlevel9k/powerlevel9k/tree/next/segments/vi_mode
# VI Mode Normal
POWERLEVEL9K_VI_MODE_NORMAL_FOREGROUND='black'
POWERLEVEL9K_VI_MODE_NORMAL_BACKGROUND='white'
# VI Mode Insert
POWERLEVEL9K_VI_MODE_INSERT_FOREGROUND='black'
POWERLEVEL9K_VI_MODE_INSERT_BACKGROUND='lightseagreen'
# VI Mode Search
POWERLEVEL9K_VI_MODE_SEARCH_FOREGROUND='black'
POWERLEVEL9K_VI_MODE_SEARCH_BACKGROUND='magenta3'
# VI Mode Visual
POWERLEVEL9K_VI_MODE_VISUAL_FOREGROUND='black'
POWERLEVEL9K_VI_MODE_VISUAL_BACKGROUND='khaki1'



POWERLEVEL9K_MODE='nerdfont-complete'
ZSH_THEME='powerlevel9k/powerlevel9k'
